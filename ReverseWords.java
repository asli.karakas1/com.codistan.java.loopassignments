package com.codistan.java.loopassignments;

import java.util.Arrays;

public class ReverseWords {
	public static void main(String[] args) {
		/*
		 * Please create an application called reverseWords in
		 * com.codistan.java.loopassignments package. Please put the words of the
		 * following message in the reverse order(words should be backwards):
		 * "Hello World this is QA class Number 1 and we are doing great!"
		 */

		String s = "Hello World this is QA class Number 1 and we are doing great!";

		String[] myString = s.split(" ");
		System.out.println(Arrays.deepToString(myString)); // [Hello, World, this, is, QA, class, Number, 1, and, we,
															// are, doing, great!]

		for (int i = myString.length - 1; i >= 0; i--) {

			System.out.print(myString[i]);
			System.out.print(" "); // on console: great! doing are we and 1 Number class QA is this World Hello

		}

	}

}
