package com.codistan.java.loopassignments;

import java.util.Scanner;

public class GradeCalculaterUsingScannerIfElse {
	public static void main(String[] args) {
		/*Assignment 1 : Please create an application class called GradeCalculator in com.codistan.java.loopassignments package. 
		 * This application will ask for Student Name and then it will ask for the exam points he got and 
		 * the application will calculate his/her grade based on the points as following and print it to the console; 
		 * if the grade is greater than 85 the grade will be �A�, greater than 75 will be �B�,
		 *  greater than 65 will be �C�, 
		 *  greater than 55 will be �D� and 
		 *  55 and less will be �F�.
		 */

	    
	 		
		System.out.println("Please enter student name");	
		Scanner  myScanner = new Scanner(System.in);
		String studentName= myScanner.nextLine();
		
		int score = 0;
		System.out.println("Please enter your test score to calculate your grade:");
		Scanner myScanner2 = new Scanner(System.in);
		int studentScore = myScanner2.nextInt();
		
		if (studentScore>85 && studentScore<=100) {
			System.out.println("Student's grade is A, student score is: "+ studentScore);
			
		} else if(studentScore>75 && studentScore<=85) {
			System.out.println("Student's grade is B, student score is " + studentScore);
			
		} else if(studentScore>65 && studentScore<=75) {
			System.out.println("Student's grade is C, student score is " + studentScore);
			
		} else if(studentScore>55 && studentScore<=65)  {
			System.out.println("Student's grade is D,student score is " + studentScore);
		} else if(studentScore<=55 && studentScore<=65) {
			System.out.println("Student's grade is F, studen score is "+ studentScore);
		} else if(studentScore>100 || studentScore<0) {
			System.out.println("Really? Please re-enter your test score");
		}
		
		
		
			
			
			
			
			
			
		}
}
