package com.codistan.java.loopassignments;

public class TwoHundredToZeroByFive {
	public static void main(String[] args) {

		/*
		 * Please create an application called twoHundredToZeroByFive in
		 * com.codistan.java.loopassignments package that counts from 200 to 0 by 5 in
		 * three different: ways, 1- for loop, 2- while, 3- do while.
		 */

		// 1-for loop:

		for (int i = 200; i >= 0; i = i - 5) {
			System.out.println(i);

		}

		// 2- while loop:

		int y = 200;
		while (y >= 0) {
			System.out.println(y);
			y = y - 5;
		}

		// 3- do while:

		int x = 205;

		do {
			System.out.println(x = x - 5);
			if (x == 0)
				break;

		} while (x >= 0);

	}

}
