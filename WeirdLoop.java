package com.codistan.java.loopassignments;

public class WeirdLoop {

	public static void main(String[] args) {

		/*
		 * Create an application called weirdLoop in com.codistan.java.loopassignments
		 * package. Print numbers to the screen like 0, 100, 5, 95, 10, 90, 15, ..., 50
		 */

		int y = 100;

		for (int i = 0; i <= 100; i++) {
			if (i % 5 == 0)
				System.out.println(y - i + "," + i);

		}

	}

}
