package com.codistan.java.loopassignments;

import java.util.Scanner;

public class SimpleCalculator {
	/*
	 * Create an application called simpleCalculator in
	 * com.codistan.java.loopassignments package that asks this; Press 1 for
	 * addition, Press 2 for subtraction, Press 3 for multiplication, Press 4 for
	 * division. After that it will say this; please enter the first number, after
	 * getting the first user input, it will say please enter the second number,
	 * after getting this number the program should do the calculation based on the
	 * user inputs.
	 */
	public static void main(String[] args) {

		System.out.println("Press 1 for addition");
		System.out.println("Press 2 for subtraction");
		System.out.println("Press 3 for multiplication");
		System.out.println("Press 4 for division");

		int addition = 1;
		int subtraction = 2;
		int multiplication = 3;
		int division = 4;

		Scanner myScanner = new Scanner(System.in);
		int calc = myScanner.nextInt();

		System.out.println("Please enter your first number:");
		int firstNumber = myScanner.nextInt();

		System.out.println("Please enter your second number:");
		int secondNumber = myScanner.nextInt();

		if (calc == addition) {
			System.out.println(firstNumber + secondNumber);
		} else if (calc == subtraction) {
			System.out.println(firstNumber - secondNumber);
		} else if (calc == multiplication) {
			System.out.println(firstNumber * secondNumber);
		} else if (calc == division) {
			System.out.println(firstNumber / secondNumber);
		} else if (calc != addition && calc != subtraction && calc != multiplication && calc != division) {
			System.out.println("Your calculator selection entry is invalid.");

		}
	}
}
