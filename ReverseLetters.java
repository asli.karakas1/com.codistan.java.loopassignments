package com.codistan.java.loopassignments;

public class ReverseLetters {
	public static void main(String[] args) {
		/*
		 * Please create an application called reverseLetters in
		 * com.codistan.java.loopassignments package which will print the following
		 * message backwards (letters should be backwards):
		 */

		String s = "Hello World this is QA class Number 1 and we are doing great!";

		// convert a string into character array and save it in a variable:
		char arr[] = s.toCharArray();
		// check if the string is empty or null:
		if (s.isEmpty() || s == null) {
			System.out.println("String is empty or not accepted");
			// print the string in reverse order:
		}
		for (int i = arr.length - 1; i >= 0; i--)

			System.out.print(arr[i]);
		System.out.println(" ");

		// !taerg gniod era ew dna 1 rebmuN ssalc AQ si siht dlroW olleH printed on
		// console

		/*
		 * logic behind: firstly, I converted the sentence to char type of an array,
		 * then conditioned arr.length-1; which is number of characters in the array(in
		 * this case 49), minus 1(as index starts from 0, our last letter can be
		 * displayed with index[48] . This way it started pulling from the top index
		 * number which is the last letter in the sentence. And it continued pulling and
		 * displaying characters until it completed index number 0, which is the "H"
		 * letter.
		 */

	}
}
